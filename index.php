<?php

// Use Markdown function from PHP Markdown http://michelf.ca/projects/php-markdown
require_once('markdown.php');

// Find the name of the script.  This is used for the database and page title
$info = pathinfo($_SERVER['SCRIPT_NAME']);
$scriptName = ucfirst($info['filename']);

// Open database, create it if it isn't there
openDatabase();
if (isset($_POST['edit'])) {
	updateContent($_POST['title'],$_POST['content'],$_POST['id']);
} 

if (isset($_GET['edit'])) {
	pageHeader('Edit or Update');
	showContentForm($_GET['edit'], $_GET['id']);
	showContent();
}
else {
	pageHeader($scriptName);
	showSearchForm();
	showContent($_GET['search']);
}
pageFooter();

$db = NULL;

// Functions
function openDatabase() {
	global $messages;
	global $db;
	global $scriptName;
	$dbFile = 'sqlite:' . $scriptName . '.data';
	try {
		$db = new PDO($dbFile);
		$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
		$db->beginTransaction();
		// try to find data
		$q = $db->query("SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'data'");
		// if no row then create table
		if ($q->fetch() === false) {
			$result = $db->exec("
			CREATE TABLE data (
			id INTEGER PRIMARY KEY,
			title TEXT,
			content TEXT
			)"
			);
			$db->commit();
			$messages[] = 'Table created';
		} 
	} catch (Exception $e) {
		echo "Database problem: " . $e->getMessage();
	}
}
	
function pageHeader($title) { 
	global $messages;
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<link type="text/css" rel="stylesheet" href="style.css" media="all" />
		<title><?php echo htmlentities($title) ?></title>
	</head>
	<body><div id="container">
	<h1><?php echo htmlentities($title) ?></h1>
	<?php
	if (isset($messages)) {
		?><ul id='messages'><?php
		foreach ($messages as $message) {
			echo "<li>" . $message . "</li>";
			}
		?></ul><?php
		}
}

function pageFooter() { ?>
	</div>
	</body>
	</html>
	<script type="text/javascript">
		
	</script>
	<?php 
}

function showContent($search = NULL) { 
	global $db;
	echo("<h2>Cards <a id='new' href='#contentForm'>new<a></h2>");
	if (isset($search)) {
		$st = $db->prepare("SELECT id,title,content FROM data WHERE title LIKE ? OR content LIKE ?ORDER BY title");
		$st->execute(array("%".$search."%", "%".$search."%"));
	}
	else {
		$st = $db->query('SELECT id,title, content FROM data ORDER BY title');
	}
	
	echo "<ul class='cards'>";
	foreach ($st->fetchALL() as $row) {
		$editLink =  "  <a class='edit' href='" . htmlentities($_SERVER['SCRIPT_NAME']) . "?edit=true&id=" . $row['id'] . "'>(edit)</a>";
		echo "<li class='title'>";
		echo Markdown($row['title'] . $editLink);
		echo "</li>";
		if ($row['content'] != '') {
			echo "<li class='content'>";
			echo Markdown($row['content']);
			echo "</li>";
		}
	}
	echo '</ul>';
	showContentForm();
}

function showSearchForm() {
	?><h2>Search Cards</h2>
	<form method="get" action="<?php echo htmlentities($_SERVER['SCRIPT_NAME']) ?>">
	<div>
	<input type="text" name="search" value="" />
	<input type="submit" value="Search" />
	</div></form><?php
}

function showContentForm($edit = NULL, $id = NULL) {
	if (isset($edit)) {
		global $db;
		$st = $db->prepare("SELECT title, content FROM data WHERE id LIKE ?");
		$st->execute(array($id));
		$content = $st->fetch();
		?><h2>Update Record <?php echo $id ?></h2>
		<form id='contentForm' method='post' action='<?php echo htmlentities($_SERVER['SCRIPT_NAME']) ?>'>
		<div>
		<input type='hidden' name='edit' value='true' />
		<input type='hidden' name='id' value='<?php echo $id; ?>' />
		<textarea name="title" cols='60' rows = '3'><?php echo $content['title']; ?></textarea>
		<textarea name='content' cols='60' rows='20'><?php echo $content['content']; ?></textarea>
		<br />
		<input type='submit' value='Save' />
		</div>
		</form>
		<?php 
	} else {
		?><h2>Insert New Card</h2>
		<form id='contentForm' method='post' action='<?php echo htmlentities($_SERVER['SCRIPT_NAME']) ?>'>
		<input type='hidden' name='edit' value='true' />
		<textarea name="title" cols='60' rows = '3'></textarea>
		<textarea name='content'  cols='60' rows='20'></textarea>
		<br />
		<input type='submit' value='Save' />
		</form>
		<?php 
	}
}		
	
function updateContent($title, $content,$id) {
	global $db;
	global $messages;
	if (isset($id)) {
		$st = $db->prepare("UPDATE data SET title = ? WHERE id LIKE ?");
		$st->execute(array($title,$id));
		$st = $db->prepare("UPDATE data SET content = ? WHERE id LIKE ?");
		$st->execute(array($content,$id));
		//$st->execute(array('content',$content,$id));
		$messages[] = "Record " . $id . " updated";
	} else {
		$st = $db->prepare("INSERT INTO data (title, content) VALUES (?,?)");
		$st->execute(array($title,$content));
		$messages[] = "New data added";
	}
	$db->commit();
} ?>
